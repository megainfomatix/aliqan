package aliqan.megainfomatix.com;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class Card extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(Card.this,Dashboard.class);
        startActivity(intent);
        finish();
    }
}
