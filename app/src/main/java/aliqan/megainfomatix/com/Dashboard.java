package aliqan.megainfomatix.com;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Dashboard extends AppCompatActivity {

    //TextView
    TextView tv_profile,tv_visitor_profile,tv_card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        //TextView
        tv_profile          = (TextView)findViewById(R.id.textView12);
        tv_visitor_profile  = (TextView)findViewById(R.id.textView13);
        tv_card             = (TextView)findViewById(R.id.textView14);

        tv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(),Profile_Own_View_Activity.class);
                startActivity(i);
                finish();

            }
        });

        tv_visitor_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(),Profile_Visitor_View_Activity.class);
                startActivity(i);
                finish();

            }
        });

        tv_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(),Card.class);
                startActivity(i);
                finish();

            }
        });

    }
}
