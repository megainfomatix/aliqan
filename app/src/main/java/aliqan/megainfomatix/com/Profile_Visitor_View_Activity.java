package aliqan.megainfomatix.com;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Profile_Visitor_View_Activity extends AppCompatActivity {

    //ImageView
    ImageView iv_back;

    // TextView
    TextView name , place , tv_head , tv_degree , tv_work , tv_attended ,tv_hosted;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_visitor_view_activity);

        Typeface typeFace= Typeface.createFromAsset(getAssets(),"fonts/roboto_black.ttf");

        // TextView
        name        = (TextView)findViewById(R.id.textView1);
        place       = (TextView)findViewById(R.id.textView2);
        tv_degree   = (TextView)findViewById(R.id.textView5);
        tv_work     = (TextView)findViewById(R.id.textView6);
        tv_attended = (TextView)findViewById(R.id.textView7);
        tv_hosted   = (TextView)findViewById(R.id.textView8);
        tv_head     = (TextView)findViewById(R.id.tv_tag);

        // Setting Typeface

        name.setTypeface(typeFace);
        place.setTypeface(typeFace);
        tv_degree.setTypeface(typeFace);
        tv_work.setTypeface(typeFace);
        tv_attended.setTypeface(typeFace);
        tv_hosted.setTypeface(typeFace);
        tv_head.setTypeface(typeFace);

        // ImageView

        iv_back = (ImageView)findViewById(R.id.imageView2);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(getApplicationContext(),Dashboard.class);
        startActivity(intent);
        finish();

    }
}
