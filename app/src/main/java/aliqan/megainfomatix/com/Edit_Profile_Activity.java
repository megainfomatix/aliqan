package aliqan.megainfomatix.com;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Edit_Profile_Activity extends Activity {


    //Button
    Button btn_save;

    //TextView
    TextView tv_name , tv_place, tv_description;

    // Image View
    ImageView back_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_activity);

        //Hiding keyboard on page view
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Typeface typeFace= Typeface.createFromAsset(getAssets(),"fonts/roboto_black.ttf");

        //TextView
        tv_name           = (TextView)findViewById(R.id.name_txt);
        tv_place          = (TextView)findViewById(R.id.sex_and_age_txt);
        tv_description    = (TextView)findViewById(R.id.description);

        //Button
        btn_save    =(Button)findViewById(R.id.save);

        //Setting Typeface
        tv_name.setTypeface(typeFace);
        tv_place.setTypeface(typeFace);
        btn_save.setTypeface(typeFace);
        tv_description.setTypeface(typeFace);



        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // Back button

        back_btn  =(ImageView)findViewById(R.id.imageView2);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Edit_Profile_Activity.this,Profile_Own_View_Activity.class);
                startActivity(intent);
                finish();

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(Edit_Profile_Activity.this,Profile_Own_View_Activity.class);
        startActivity(intent);
        finish();
    }
}
