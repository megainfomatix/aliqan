package aliqan.megainfomatix.com;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Profile_Own_View_Activity extends Activity {

    //ImageView
    ImageView iv_edit,iv_back;

    // TextView
    TextView name , place , tv_head , tv_degree , tv_work , tv_attended ,tv_hosted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_own_view_activity);

        Typeface typeFace= Typeface.createFromAsset(getAssets(),"fonts/roboto_black.ttf");

        // Image View
        iv_edit = (ImageView)findViewById(R.id.imageView);
        iv_back = (ImageView)findViewById(R.id.imageView2);

        iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Profile_Own_View_Activity.this,Edit_Profile_Activity.class);
                startActivity(intent);
                finish();
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        // TextView
        name    = (TextView)findViewById(R.id.textView1);
        place   = (TextView)findViewById(R.id.textView2);
        tv_head = (TextView)findViewById(R.id.tv_tag);
        tv_degree = (TextView)findViewById(R.id.textView5);
        tv_work = (TextView)findViewById(R.id.textView6);
        tv_attended = (TextView)findViewById(R.id.textView7);
        tv_hosted = (TextView)findViewById(R.id.textView8);

        //Setting Typeface

        name.setTypeface(typeFace);
        place.setTypeface(typeFace);
        tv_head.setTypeface(typeFace);
        tv_degree.setTypeface(typeFace);
        tv_work.setTypeface(typeFace);
        tv_attended.setTypeface(typeFace);
        tv_hosted.setTypeface(typeFace);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(getApplicationContext(),Dashboard.class);
        startActivity(intent);
        finish();
    }
}
