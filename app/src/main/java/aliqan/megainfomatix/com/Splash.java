package aliqan.megainfomatix.com;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class Splash extends AppCompatActivity {

    final Handler handler = new Handler();
    private static int SPLASH_TIME_OUT = 2000; // Splash screen timer

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(Splash.this,Dashboard.class);
                startActivity(i);
                finish();

            }
        }, SPLASH_TIME_OUT);

    }
}
